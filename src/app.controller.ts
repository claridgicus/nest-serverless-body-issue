import { Controller, Get, Post, Body, Req } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('healthcheck')
  healthcheck() {
    return this.appService.healthcheck();
  }

  @Get('test')
  async blob(@Req() req) {
    return req;
  }

  @Post('test')
  async storeSessions(@Body() body) {
    return body;
  }
}
